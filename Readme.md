### `Tool purpose, features:` 
- `extract email addresses from MBOX files;`
- `multiprocessing, splits the file in multiple parts so that it finishes faster;`
- `configurable number of processes by setting the variable: split_number , 
this variable is used to split the files but it also sets indirectly the 
number of processes used to parse and extract data;`

### `Tool usage example:`
emadrext.py "email archive.MBOX"

the tool takes 1 parameter, a full path given, enclose it in ""




### `Speed test results for a MBOX of 380 MB system with 8CPU cores, profiler results:`
- with 1 process: 34.9s
- with 8 processes: 6.2s
- with 15 processes: 6.3s

increasing the number of processes too much without additional CPU cores 
does not seem to help gain any performance increase.

### `How to easily obtain a MBOX file?`
- go to https://takeout.google.com/ and create an export (just for gmail)