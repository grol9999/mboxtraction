import re
import sys
import os
from multiprocessing import Process, Manager

# File path
file_path_input = sys.argv[1]

# Number of file splits and processes
split_number = 15

# Contains a list of lists for file offsets:
# [[start_offset_0, end_offset_0], [start_offset_1, end_offset_1]]
s1 = []

chunk_size = int(os.stat(file_path_input).st_size/split_number)

# Generate the offset list
for i in range(0, split_number):
    s1.append([i*chunk_size, (i+1)*chunk_size])


class FileClass:
    def __init__(self, filepath, offset):
        my_file = open(filepath, "r", encoding="utf-8")
        my_file.seek(offset[0])
        self.file_pointer = my_file
        self.file_offset = offset[0]

    def read_line_from_offset(self):
        data_chunk = self.file_pointer.readline()
        self.file_offset += len(data_chunk) + 1
        return data_chunk

    def get_current_offset(self):
        return self.file_offset


def read_file(file_path, offs_read, eml_dict):
    # DEBUG PRINT
    # print("File path {} , offset {}".format(file_path_input, offs_read))

    email_regex_chk = ['^To:.*?([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)',
                       'From:.*?([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)',
                       'Sender:.*?([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)']

    my_file = FileClass(file_path, offs_read)
    my_file_r = my_file.read_line_from_offset()
    while my_file.get_current_offset() < offs_read[1]:
        # DEBUG PRINT
        # print("Current offset {} , max ofset {}".format(my_file.get_current_offset(), offs_read[1]))
        try:
            regex_chk = []
            for r_chk in email_regex_chk:
                regex_chk = regex_chk + re.findall(r_chk, my_file_r)

            for email_found in regex_chk:
                if email_found != '':
                    if email_found not in eml_dict.keys():
                        eml_dict[email_found] = 1
                    else:
                        eml_dict[email_found] += 1

            my_file_r = my_file.read_line_from_offset()
        except Exception as err:
            print(err)
            print(my_file_r)


if __name__ == '__main__':
    with Manager() as manager:
        multi_procs = []
        my_email_dict = manager.dict()

        for i in s1:
            proc = Process(target=read_file, args=[file_path_input, i, my_email_dict])
            multi_procs.append(proc)
        for l_proc in multi_procs:
            l_proc.start()

        for l_proc in multi_procs:
            l_proc.join()

        print(my_email_dict)
